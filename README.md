# Dockerized CI/CD with GitLab

## Introduction
This project is a simple example of how to use GitLab CI/CD to build and deploy a simple web application. The application is a simple web page that displays the Hello World. The application is built using the [Vite](https://vitejs.dev/) and [TypeScript](https://www.typescriptlang.org/) frameworks.

Here's a screenshot of project tree:
![screen-1.png](screens/screen-1.png)

If you want to run the application locally:

- Clone the repository
- Be sure that you have [Node.js](https://nodejs.org/en/) installed
- Go to the project directory
- Run `npm install` to install the dependencies
- Run `npm run dev` to start the application

![screen-2.png](screens/screen-2.png)

## Dockerizing the application

The application is dockerized using the [Dockerfile](Dockerfile) file. The Dockerfile is a multi-stage build that uses the [Node.js](https://nodejs.org/en/) image to build the application and run it.

![screen-3.png](screens/screen-3.png)

## GitLab CI/CD

The GitLab CI/CD is configured using the [.gitlab-ci.yml](.gitlab-ci.yml) file. The file contains the following stages:

- `build` - builds the application using the [Dockerfile](Dockerfile) file
- `test` - runs the tests
- `deploy` - deploys the application

![screen-4.png](screens/screen-4.png)

## Deploying the application

WIP (work in progress)

## Testing the application

After you did all the steps at [Introduction](#introduction), you can test the application by running the following command:

```bash
npm test
```

![screen-5.png](screens/screen-5.png)