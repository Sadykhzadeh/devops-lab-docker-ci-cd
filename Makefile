build:
	docker build -t devops .
start:
	docker run --rm -p3000:4173 -d --name devops devops
stop:
	docker stop devops