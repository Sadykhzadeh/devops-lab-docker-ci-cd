FROM node:latest

# Set the working directory for the container
WORKDIR /app

# Copy the app's code to the container's working directory
COPY . .

# Install the app's dependencies
RUN npm install

# Run the app's tests
RUN npm run test

# Build the app
RUN npm run build

# Expose the port that the app will be running on
EXPOSE 4173

# Start the app
CMD ["npm", "run", "preview"]